homeSectionShow();
const sections = [
  'home-section',
  'about-us-section',
  'profiles-section',
  'contact-section',
];
const animationShow = [
  homeSectionShow,
  aboutUsSectionShow,
  profileSectionShow,
  contactSectionShow,
];
const animationHide = [
  homeSectionHide,
  aboutUsSectionHide,
  profileSectionHide,
  contactSectionHide,
];
let currentPageId = 0;
let lastScroll = 0;

function openMobileMenu() {
  const menu = document.getElementById('mobileMenu');
  menu.classList.remove('animation-hideDownToRight');
  menu.classList.add('animation-slideUpFromRight');
}
function closeMobileMenu() {
  const menu = document.getElementById('mobileMenu');
  menu.classList.remove('animation-slideUpFromRight');
  menu.classList.add('animation-hideDownToRight');
}
window.addEventListener('wheel', (e) => {
  const currentTime = new Date().getTime();

  if (currentTime - lastScroll < 500) {
    return;
  }

  const delta = e.deltaY;

  if (delta > 0 && currentPageId < sections.length - 1) {
    animationHide[currentPageId]();
    currentPageId++;
    lastScroll = currentTime;
    let link = document.createElement('a');

    animationShow[currentPageId]();
    link.href = `#${sections[currentPageId]}`;
    link.click();
  }
  if (delta < 0 && currentPageId > 0) {
    animationHide[currentPageId]();
    currentPageId--;
    lastScroll = currentTime;
    let link = document.createElement('a');

    animationShow[currentPageId]();
    link.href = `#${sections[currentPageId]}`;
    link.click();
  }
});

function homeSectionShow() {
  /* document.getElementById('homeSectionTitle').animate(
    [
      { transform: 'translateY(-100px)', opacity: 0 },
      { transform: 'translateY(0px)', opacity: 1 },
    ],
    {
      duration: 1000,
      fill: 'forwards',
    }
  ); */
  document.getElementById('homeSectionTexts').children[0].animate(
    [
      { transform: 'translateX(-100px)', opacity: 0 },
      { transform: 'translateX(0px)', opacity: 1 },
    ],
    {
      duration: 1000,
      fill: 'forwards',
      delay: 0,
    }
  );
  document.getElementById('homeSectionTexts').children[1].animate(
    [
      { transform: 'translateX(100px)', opacity: 0 },
      { transform: 'translateX(0px)', opacity: 1 },
    ],
    {
      duration: 1000,
      fill: 'forwards',
      delay: 500,
    }
  );
  document.getElementById('homeSectionTexts').children[2].animate(
    [
      { transform: 'translateY(100px)', opacity: 0 },
      { transform: 'translateY(0px)', opacity: 1 },
    ],
    {
      duration: 1000,
      fill: 'forwards',
      delay: 1500,
    }
  );
}

function homeSectionHide() {
  const elements = document.getElementById('homeSectionTexts').children;

  [...elements].forEach((item) => {
    item.animate([{ opacity: 1 }, { opacity: 0 }], {
      duration: 1,
      fill: 'forwards',
    });
  });
}

function aboutUsSectionShow() {
  /* document.getElementById('aboutSectionTitle').animate(
    [
      { opacity: 0, transform: 'translateY(-50px)' },
      { opacity: 1, transform: 'translateY(0px)' },
    ],
    {
      duration: 1000,
      fill: 'forwards',
    }
  ); */
  const elements = document.getElementById('aboutSectionTexts').children;
  [...elements].forEach((item, i) => {
    item.animate(
      [
        { opacity: 0, transform: 'translateY(-50px)' },
        { opacity: 1, transform: 'translateY(0px)' },
      ],
      {
        duration: 1000,
        fill: 'forwards',
        delay: (i + 1) * 500,
      }
    );
  });
}

function aboutUsSectionHide() {
  const elements = document.getElementById('aboutSectionTexts').children;
  [...elements].forEach((item) => {
    item.animate([{ opacity: 1 }, { opacity: 0 }], {
      duration: 1,
      fill: 'forwards',
    });
  });
}

function profileSectionShow() {
  /* document.getElementById('profileSectionTitle').animate(
    [
      { opacity: 0, transform: 'translateY(50px)' },
      { opacity: 1, transform: 'translateY(0px)' },
    ],
    {
      duration: 1000,
      fill: 'forwards',
    }
  ); */
  const elements = document.getElementById('profileSectionTextsA').children;
  [...elements].forEach((item, i) => {
    item.animate(
      [
        { opacity: 0, transform: 'translateY(50px)' },
        { opacity: 1, transform: 'translateY(0px)' },
      ],
      {
        duration: 500,
        fill: 'forwards',
        delay: (i + 1) * 250,
      }
    );
  });
  const elementsB = document.getElementById('profileSectionTextsB').children;
  [...elementsB].forEach((item, i) => {
    item.animate(
      [
        { opacity: 0, transform: 'translateY(50px)' },
        { opacity: 1, transform: 'translateY(0px)' },
      ],
      {
        duration: 500,
        fill: 'forwards',
        delay: 1500 + (i + 1) * 250,
      }
    );
  });
}

function profileSectionHide() {
  const elements = document.getElementById('profileSectionTextsA').children;
  [...elements].forEach((item) => {
    item.animate([{ opacity: 1 }, { opacity: 0 }], {
      duration: 1,
      fill: 'forwards',
    });
  });
  const elementsB = document.getElementById('profileSectionTextsB').children;
  [...elementsB].forEach((item) => {
    item.animate([{ opacity: 1 }, { opacity: 0 }], {
      duration: 1,
      fill: 'forwards',
    });
  });
}

function contactSectionShow() {
  /* document.getElementById('contactSectionTitle').animate(
    [
      { opacity: 0, transform: 'translateY(50px)' },
      { opacity: 1, transform: 'translateY(0px)' },
    ],
    {
      duration: 1000,
      fill: 'forwards',
    }
  ); */
  const elements = document.getElementById('contactSectionTexts').children;
  [...elements].forEach((item, i) => {
    item.animate(
      [
        { opacity: 0, transform: 'translateY(50px)' },
        { opacity: 1, transform: 'translateY(0px)' },
      ],
      {
        duration: 1000,
        fill: 'forwards',
        delay: (i + 1) * 500,
      }
    );
  });
}

function contactSectionHide() {
  const elements = document.getElementById('contactSectionTexts').children;
  [...elements].forEach((item) => {
    item.animate([{ opacity: 1 }, { opacity: 0 }], {
      duration: 1,
      fill: 'forwards',
    });
  });
}

function hideAll() {
  homeSectionHide();
  aboutUsSectionHide();
  profileSectionHide();
  contactSectionHide();
}

let touchstartY = 0;
let touchendY = 0;

function handleGesture() {
  console.log(currentPageId);
  if (touchendY < touchstartY && currentPageId < sections.length - 1) {
    animationHide[currentPageId]();
    currentPageId++;
    //lastScroll = currentTime;
    let link = document.createElement('a');

    animationShow[currentPageId]();
    link.href = `#${sections[currentPageId]}`;
    link.click();
  }
  if (touchendY > touchstartY && currentPageId > 0) {
    animationHide[currentPageId]();
    currentPageId--;
    //lastScroll = currentTime;
    let link = document.createElement('a');

    animationShow[currentPageId]();
    link.href = `#${sections[currentPageId]}`;
    link.click();
  }
}

window.addEventListener('touchstart', (e) => {
  touchstartY = e.changedTouches[0].screenY;
});

window.addEventListener('touchend', (e) => {
  touchendY = e.changedTouches[0].screenY;
  handleGesture();
});
